package  {
	
	public class AssetsAdd {
		
		[Embed(source="assets/play_btn_up.png")]
		public static const play_btn_up: Class;
		
		[Embed(source="assets/play_btn_down.png")]
		public static const play_btn_down: Class;
		
		public static const Cat_Screech_Sound: String = "assets/Cat_Screech_Sound_Effect.mp3";
		public static const Love_Cats_Sound: String = "assets/LoveCats.mp3";
		
	}
}