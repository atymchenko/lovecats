package assets {
	
	public class AssetsParticles {

		[Embed(source="blood_particle.pex", mimeType="application/octet-stream")]
		public static const BloodConfig:Class;
		
		[Embed(source = "blood_texture.png")]
		public static const BloodParticle:Class;
		
	}
}