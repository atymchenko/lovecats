package view {
	import controller.AssetsManager;
	import controller.TouchManager;
	
	import org.gestouch.events.GestureEvent;
	import org.gestouch.gestures.TapGesture;
	
	import starling.display.Button;
	import starling.display.Sprite;
	
	public class ToolPanel extends Sprite {
		
		public var gap: Number = 100;
		
		private var buttons: Vector.<Button>;
		
		private var assetManager: AssetsManager;
		
		public function ToolPanel() {
			super();
			buttons = new Vector.<Button>();
			assetManager = AssetsManager.getInstance();
			init();
		}
		
		private function init(): void {
			var swordButton: Button = assetManager.swordButton;
			var hammerButton: Button = assetManager.hammerButton;
			buttons.push(swordButton);
			buttons.push(hammerButton);
			update();
			
			var swordTapGesture: TapGesture = new TapGesture(swordButton);
			var hammerTapGesture: TapGesture = new TapGesture(hammerButton);
			
			//tapGesture.numTapsRequired = 1;
			swordTapGesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onTabSword);
			hammerTapGesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onTabHammer);
			
			touchManager = TouchManager.getInstance();
		}
		private var touchManager: TouchManager;
		
		private function onTabSword(event: GestureEvent): void {
			touchManager.type = TouchManager.CUT_TYPE;
		}
		
		private function onTabHammer(event: GestureEvent): void {
			touchManager.type = TouchManager.HIT_TYPE;
		}
		
		private function update(): void {
			removeChildren();
			for (var i: int = 0; i < buttons.length; i++) {
				var button: Button = buttons[i];
				button.x = (button.width + gap) * i;
				addChild(button);
			}
		}
	}
}