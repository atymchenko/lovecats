package {
	
	import starling.display.Sprite;
	import starling.text.TextField;
	import model.MainModel;
	
	public class HUDPanel extends Sprite {
		
		private var mainModel: MainModel;
		
		private var textField: TextField;
		
		public function HUDPanel(model: MainModel) {
			super();
			this.mainModel = model;
			init();
		}
		
		private function init(): void {
			textField = new TextField(200, 50, "", "Verdana", 30, 0xffffff);
			addChild(textField);
			update();
		}
		
		public function update(): void {
			var label: String = mainModel.points.toString(); 
			textField.text = "x" + label; //getPrefix(label.length)
			
			function getPrefix(len: int, maxLen: int = 9): String {
				var pre: String = "";
				for (var i: int = 0 ; i < maxLen - len; i++) {
					pre += "0";
				}
				return pre;
			}
			
		}
	}
}