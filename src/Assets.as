package  {
	
	public class Assets {
		
		/*[Embed(source="assets/kill_cat_1.png")]
		public static const kill_cat_1: Class;
		
		[Embed(source="assets/kill_cat_1.xml", mimeType="application/octet-stream")]
		public static const xml_kill_cat_1: Class;*/
		
		[Embed(source="assets/button.png")]
		public static const button: Class;
		
		[Embed(source="assets/button.xml", mimeType="application/octet-stream")]
		public static const xml_button: Class;
		
		[Embed(source="assets/bread.png")]
		public static const bread: Class;
		
		[Embed(source="assets/bread.xml", mimeType="application/octet-stream")]
		public static const xml_bread: Class;
	}
}