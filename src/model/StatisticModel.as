package model {
	
	public class StatisticModel {
		
		private var killedCats: Vector.<CatVO>;
		
		public function StatisticModel() {
			killedCats = new Vector.<CatVO>();
		}
		
		public function get killsNum(): int {
			return killedCats.length;
		}
		
		public function addKillsData(cat: CatVO): void {
			killedCats.push(cat);
		}
		
	}
}