package model {
	
	public class CatVO {
		
		public var type: String;
		
		public function CatVO(type: String) {
			this.type = type;
		}
		
		public function clone(): CatVO {
			return new CatVO(type);
		}
		
	}
}