package model {
	import flash.media.Sound;
	import flash.media.SoundChannel;
	
	public class SoundVO {
		
		public var path: String;
		public var sound: Sound;
		public var soundChannel: SoundChannel;
		
		public function SoundVO(path: String, sound: Sound) {
			this.path = path;
			this.sound = sound;
		}
	}
}