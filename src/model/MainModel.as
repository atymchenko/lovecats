package model {
	
	public class MainModel {

		public var points: Number = 0;
		
		public function MainModel() {
		}
		
		private var _catsNum: int = 0;
		
		public function get catsNum(): int {
			return _catsNum;
		}
		
		public function addCat(): void {
			_catsNum++;
		}
		
		private static var instance: MainModel;
		public static function getInstance(): MainModel {
			if (!instance) {
				instance = new MainModel();
			}
			return instance;
		}
	}
}