package {
	import flash.display.Sprite;
	import flash.geom.Point;
	
	import controller.AnimationManager;
	import controller.KillManager;
	
	import model.DogVO;
	
	import org.gestouch.events.GestureEvent;
	import org.gestouch.gestures.LongPressGesture;
	
	import starling.core.Starling;
	import starling.display.Quad;
	
	public class Dog extends Quad {

		private var _dogVO: DogVO;
		
		private var _isActivated: Boolean;
		
		private var killed: int;
		
		private var currentCat: Cat;
		
		public var scene: Scene;
		
		public var isBusy: Boolean;
		
		private var longPressGesture: LongPressGesture;
		private var animationManager: AnimationManager;
		private var killManager: KillManager;
		
		public function Dog(dogVO: DogVO, initPosition: Point) {
			super(20, 20, 0xffffff);
			this.dogVO = dogVO;
			this.x = initPosition.x;
			this.y = initPosition.y;
			
			longPressGesture = new LongPressGesture(this);
			longPressGesture.addEventListener(GestureEvent.GESTURE_BEGAN, onPressDogBegan);
			longPressGesture.addEventListener(GestureEvent.GESTURE_ENDED, onPressDogEnd);
			
			animationManager = AnimationManager.getInstance();
			killManager = KillManager.getInstance();
			
		}
		
		public function strikeCat(): void {
			if (isActivated) {
				currentCat = findCat();
				if (currentCat) {
					isBusy = true;
					var position: Point = estimateDistance(currentCat);
					runTo(position);
				}
			}
		}
		
		public function findCat(): Cat {
			var cat: Cat;
			var centerX: Number = this.x + this.width/2;
			var centerY: Number = this.y + this.height/2;
			for (var i: int = 0; i < scene.numChildren; i++) {
				cat = scene.getChildAt(i) as Cat;
				/*if (cat) {
					break;
				}*/
				if (cat && dogVO.range > Math.sqrt( Math.pow( (centerX - cat.x) ,2) + Math.pow( (centerY - cat.y) ,2) )) {
					return cat;
				}
			}
			return null;
		}
		
		public function killCat(): void {
			if (currentCat) {
				killManager.cutKill(currentCat);
				killed++;
				trace("Dog has killed: ", killed);
			}
			
		}

		public function runTo(position: Point): void {
			var time: Number = Math.sqrt(Math.pow(position.y-this.y,2) + Math.pow(position.x-this.x,2)) / dogVO.speed;
			animationManager.playTween(this, time, {x: position.x, y: position.y, onComplete: runCompleted});
		}
		
		public function get isActivated(): Boolean {
			return _isActivated;
		}
		
		public function set isActivated(value: Boolean): void {
			if (_isActivated != value) {
				_isActivated = value;
				if (value) {
					strikeCat();
				}
			}
		}
		
		public function get dogVO(): DogVO {
			return _dogVO;
		}
		
		public function set dogVO(value: DogVO): void {
			_dogVO = value;
		}
		
		public function destroy(): void {
			killed = 0;
			scene = null;
			dogVO = null;
			animationManager = null;
			currentCat = null;
			longPressGesture = null;
			animationManager = null;
			killManager = null;
			removeFromParent(true);
		}
		
		// cats with horiz duraction
		private function estimateDistance(cat: Cat): Point {
			var Xc: Number = cat.x;
			var Xd: Number = this.x;
			var Vc: Number = 1280/6;
			var Vd: Number = dogVO.speed;
			var Yc: Number = cat.y;
			var Yd: Number = this.y;
			
			var Vc2: Number = Math.pow(Vc,2);
			var Vd2: Number = Math.pow(Vd,2);
			var Xc2: Number = Math.pow(Xc,2);
			var Xd2: Number = Math.pow(Xd,2);
			
			var b: Number = Vd2 - Vc2;
			var c: Number = Xc - Xd;
			
			var resX: Number = c + Math.sqrt( ( Vc2*Math.pow((Yc-Yd),2) + Vd2*Xc2 + Vc2*Xd2 + b*Math.pow(c,2) ) / b );
			var resY: Number = Yc;
			
			return new Point(resX, resY);
		}
		
		private function runCompleted(): void {
			killCat();
			isBusy = false;
			strikeCat();
		}
		
		
		private var sprite: Sprite = new Sprite();
		private function onPressDogBegan(event: GestureEvent): void {
			Starling.current.nativeStage.addChild(sprite);
			sprite.graphics.lineStyle(2, 0xffffff, 0.5);
			sprite.graphics.drawCircle(this.x+this.width/2, this.y+this.height/2, dogVO.range);
		}
		
		private function onPressDogEnd(event: GestureEvent): void {
			Starling.current.nativeStage.removeChild(sprite);
		}
		
	}
}