package {
	
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.utils.Timer;
	
	import controller.AnimationManager;
	import controller.AssetsManager;
	import controller.ResultController;
	import controller.SoundManager;
	import controller.TouchManager;
	
	import model.DogVO;
	import model.MainModel;
	
	import org.gestouch.core.Gestouch;
	import org.gestouch.core.IInputAdapter;
	import org.gestouch.extensions.starling.StarlingDisplayListAdapter;
	import org.gestouch.extensions.starling.StarlingTouchHitTester;
	import org.gestouch.input.NativeInputAdapter;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.DisplayObject;
	import starling.display.MovieClip;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	
	import view.ToolPanel;
	
	public class Scene extends Sprite {
		
		public var isPlaing: Boolean;
		
		public var mainModel: MainModel;
		public var generator: CatGenerator;  
		public var soundManager: SoundManager;
		public var animationManager: AnimationManager;
		public var resultController: ResultController;
		
		public var assetManager: AssetsManager;
		
		private var hud: HUDPanel;
		private var background: Quad;
		
		private var timer: Timer;
		
		private var dog: Dog;
		
		public function Scene() {
			super();

			Gestouch.inputAdapter ||= new NativeInputAdapter(Starling.current.nativeStage) as IInputAdapter;
			Gestouch.addDisplayListAdapter(DisplayObject, new StarlingDisplayListAdapter());
			Gestouch.addTouchHitTester(new StarlingTouchHitTester(Starling.current), -1);
			
			mainModel = new MainModel();
			generator = new CatGenerator();
			soundManager = new SoundManager();
			animationManager = AnimationManager.getInstance();
			//soundManager.addSound(AssetsAdd.Love_Cats_Sound);
			resultController = new ResultController();
			
			background = new Quad(800, 600, 0x000000);
			addChild(background);
			
			hud = new HUDPanel(mainModel);
			hud.x = this.width - hud.width - 50;
			addChild(hud);
			
			assetManager = AssetsManager.getInstance();
			assetManager.addEventListener("complete", onAssetComplete);
			
			timer = new Timer(800);
			
			var button: Button = new Button(Texture.fromBitmap(new AssetsAdd.play_btn_up()), "play", Texture.fromBitmap(new AssetsAdd.play_btn_down()));
			button.addEventListener(TouchEvent.TOUCH, function(event: TouchEvent):void{
				if (event.getTouch(button, TouchPhase.ENDED)) {
					if (isPlaing) {
						stopGame();
						button.text = "play";
						soundManager.removeAllSounds();
					} else {
						startGame();
						button.text = "stop";
						//soundManager.playSound(AssetsAdd.Cat_Screech_Sound);
						soundManager.playSound(AssetsAdd.Love_Cats_Sound, 0.3);
						mainModel.points = 0;
						hud.update();
					}
					
				}
			});
			addChild(button);
			
			
			var touchManager: TouchManager = TouchManager.getInstance();
			touchManager.register(this);
			/*touchManager.component = this;
			touchManager.type = TouchManager.CUT_TYPE;*/
			
			/*var ps: ParticleManager = new ParticleManager(this);
			ps.startParticle(ps.bloodParticleSystem, new Point(300, 200));*/
			
		}
		
		private function addListeners(): void {
		}
		
		private function removeListeners(): void {
		}
		 
		private function onTimer(event: TimerEvent): void {
			var catsNum: int = generator.getCatsNumbers();
			var cat: Cat;
			for (catsNum; catsNum > 0; catsNum--) {
				cat = new Cat(getCat(), onTouchCatStartHandler);
				moveCat(cat);
			}
			if (!dog.isBusy) {
				dog.strikeCat();
			}
		}
		
		private function onAssetComplete(event: Event): void {
			assetManager.removeEventListener("complete", onAssetComplete);
			var toolPanel: ToolPanel = new ToolPanel();
			toolPanel.x = 50;
			toolPanel.y = this.height - toolPanel.height;
			addChild(toolPanel);
		}
		
		public function getCat(): MovieClip {
			return assetManager.getCatMovingMC();
		}
		
		private function onTouchCatStartHandler(cat: Cat): void {
			animationManager.stopTween(cat);
			var winPoints: Number = 100;
			playWinAnimation(winPoints, cat);
			addPoints(100);
		}
		
		private function playWinAnimation(value: Number, target: DisplayObject): void {
			var label: String = "+"+value.toString();
			var textField: TextField = new TextField(60, 40, label, "Verdana", 18, Math.round(Math.random() * 0xffffff));
			textField.x = target.x + target.width/2 - textField.width/2;
			textField.y = target.y + target.height/2 - textField.height/2;
			addChild(textField);
			animationManager.playTween(textField, 0.8, {y: textField.y - 100, alpha: 0,
				onComplete: function(): void {
					animationManager.stopTween(textField);
					textField.dispose();
				}
			});
		}
		
		private function addPoints(value: Number): void {
			mainModel.points += value;
			if (hud) {
				hud.update();
			}
		}
		
		private function moveCat(target: DisplayObject): void {
			var deviation: Number = 10;
			target.x = -(target.width+deviation);
			var rand: Number = Math.random();
			target.y = rand * (this.height - target.height - deviation);
			if (target.y < 50) {
				target.y = 50;
			}
			addChild(target);
			mainModel.addCat();
			animationManager.playTween(target, 6, {x: 1280 + deviation,
				onComplete: function(): void {
					animationManager.stopTween(target);
					target.dispose();
					if (resultController.allowedCatNum >= 0) {
						resultController.omitCat();
					} else {
						stopGame();
					}
				}
			});
		}
		
		public function startGame(): void {
			isPlaing = true;
			timer.addEventListener(TimerEvent.TIMER, onTimer);
			addListeners();
			timer.start();
			
			dog = new Dog(new DogVO("Ferdinand", 250, 1280/6 * 2, 0), new Point(500, 400));
			addChild(dog);
			dog.scene = this;
			dog.isActivated = true;
		}
		
		public function stopGame(): void {
			isPlaing = false;
			removeListeners();
			timer.removeEventListener(TimerEvent.TIMER, onTimer);
			timer.stop();
			trace("catsNum: ", mainModel.catsNum);
		}
	}
}