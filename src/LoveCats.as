package {
	
	import flash.display.Sprite;
	import flash.events.Event;
	
	import starling.core.Starling;
	
	[SWF(pageTitle="Love Cats", frameRate="60", backgroundColor="#000000", width="800", height="600")]
	public class LoveCats extends Sprite {
		
		public function LoveCats() {
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(event: Event): void {
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			var mStarling: Starling = new Starling(Scene, this.stage);
			mStarling.showStats = true;
			mStarling.showStatsAt("right");
			mStarling.start();
		}
		
	}
}