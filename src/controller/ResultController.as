package controller {
	
	public class ResultController {
		
		private var _allowedCatNum: int = 1000000;
		
		public function ResultController() {
		}
		
		public function omitCat(): void {
			_allowedCatNum--;
		}
		
		public function get allowedCatNum(): int {
			return _allowedCatNum;
		}
	}
}