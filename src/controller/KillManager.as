package controller {
	
	import starling.display.MovieClip;
	
	public class KillManager {
		
		private var killed: int;
		
		private var assetManager: AssetsManager;
		private var animationManager: AnimationManager;
		
		public function KillManager() {
			animationManager = AnimationManager.getInstance();
			assetManager = AssetsManager.getInstance();
		}
		
		public function hitKill(cat: Cat): void {
			if (cat.isDisposed) return;
			cat.kill();
			var mc: MovieClip = assetManager.getCatHitKillMC({x: cat.x, y: cat.y});
			if (mc) {
				cat.parent.addChild(mc);
				animationManager.playMC(mc, false, killAnimCompleted);
			}
			kill(cat);
		}
		
		public function cutKill(cat: Cat): void {
			if (cat.isDisposed) return;
			cat.kill();
			var mc: MovieClip = assetManager.getCatCutKillMC({x: cat.x, y: cat.y});
			if (mc) {
				cat.parent.addChild(mc);
				animationManager.playMC(mc, false, killAnimCompleted);
			}
			kill(cat);
		}
		
		public function kill(cat: Cat): void {
			cat.dispose();
			killed++;
		}
		
		private function killAnimCompleted(mc: MovieClip): void {
			mc.removeFromParent(true);
		}

		public function destroy(): void {
			killed = 0;
			assetManager = null;
			animationManager = null;
			assetManager = null;
			instance = null;
		}
		
		private static var instance: KillManager;
		public static function getInstance(): KillManager {
			if (!instance) {
				instance = new KillManager();
			}
			return instance;
		}
		
	}
}