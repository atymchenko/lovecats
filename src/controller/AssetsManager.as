package controller {
	
	import starling.display.Button;
	import starling.display.MovieClip;
	import starling.events.Event;
	import starling.events.EventDispatcher;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	
	[Event(name="complete", type="flash.events.Event")]
	public class AssetsManager extends EventDispatcher {
		
		private var assetManager: AssetManager;
		
		public function AssetsManager() {
			assetManager = new AssetManager();
			assetManager.verbose = true;
			assetManager.enqueue(Assets);
			assetManager.loadQueue(onAssetComplete);
		}
		
		public function destroy(): void {
			
			assetManager.dispose();
			assetManager = null;
			instance = null;
		}
		
		public function getCatMovingMC(): MovieClip {
			var mc: MovieClip;
			var textures: Vector.<Texture> = assetManager.getTextures("moving");
			if (textures.length > 0) {
				mc = new MovieClip(textures);
				mc.fps = 24;
				mc.width /= 2;
				mc.height /= 2;
			}
			return mc;
		}
		public function getCatCutKillMC(properties: Object = null): MovieClip {
			var mc: MovieClip;
			var textures: Vector.<Texture> = assetManager.getTextures("kill_cut");
			if (textures.length > 0) {
				mc = new MovieClip(textures);
				mc.fps = 60;
				mc.width /= 2;
				mc.height /= 2;
				
				if (properties) {
					for (var propName: String in properties) {
						mc[propName] = properties[propName];
					}
				}
				
			}
			return mc;
		}
		public function getCatHitKillMC(properties: Object = null): MovieClip {
			var mc: MovieClip;
			var textures: Vector.<Texture> = assetManager.getTextures("body_hit");
			if (textures.length > 0) {
				mc = new MovieClip(textures);
				mc.fps = 36;
				mc.width /= 2;
				mc.height /= 2;
				
				if (properties) {
					for (var propName: String in properties) {
						mc[propName] = properties[propName];
					}
				}
				
			}
			return mc;
		}
		public function get swordButton(): Button {
			var swordIconTextures: Vector.<Texture> = assetManager.getTextures("swordIcon");
			var swordToolBtn: Button = new Button(swordIconTextures[0]);
			return swordToolBtn;
		}
		public function get hammerButton(): Button {
			var hammerIconTextures: Vector.<Texture> = assetManager.getTextures("hammerIcon");
			var hammerToolBtn: Button = new Button(hammerIconTextures[0]);
			return hammerToolBtn;
		}
		
		
		
		private function onAssetComplete(ratio: Number): void {
			if (ratio == 1) {
				dispatchEvent(new Event("complete"));
			}
		}
		
		private static var instance: AssetsManager;
		public static function getInstance(): AssetsManager {
			if (!instance) {
				instance = new AssetsManager();
			}
			return instance;
		}
	}
}