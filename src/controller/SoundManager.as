package controller {
	import flash.events.Event;
	import flash.media.Sound;
	import flash.media.SoundTransform;
	import flash.net.URLRequest;
	import flash.utils.Dictionary;
	
	import model.SoundVO;
	
	public class SoundManager {
		
		private var sounds: Dictionary;
		
		public function SoundManager() {
			sounds = new Dictionary(true);
		}
		
		public function addSound(path: String): SoundVO {
			var soundVO: SoundVO = sounds[path] as SoundVO;
			if (!soundVO) {
				var sound: Sound = new Sound(new URLRequest(path));
				sound.addEventListener(Event.COMPLETE, function(e:Event):void{
					trace("sound complete:","url=",sound.url,", bytes=",sound.bytesTotal);
				});
				soundVO = new SoundVO(path, sound);
				sounds[path] = soundVO;
			}
			return soundVO;
		}
		
		public function playSound(path: String, volume: Number = 1): void {
			var soundVO: SoundVO = addSound(path);
			soundVO.soundChannel = soundVO.sound.play(0, 0, new SoundTransform(volume));
			soundVO.soundChannel.addEventListener(Event.SOUND_COMPLETE, onSoundComplete);
		}
		
		public function pauseSound(path: String): void {
			/*var soundVO: SoundVO = sounds[path] as SoundVO;
			if (soundVO) {
				soundVO.soundChannel.stop();
			}*/
		}
		
		public function stopSound(path: String): void {
			var soundVO: SoundVO = sounds[path] as SoundVO;
			if (soundVO) {
				soundVO.soundChannel.stop();
				soundVO.soundChannel.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);
				delete sounds[path];
			}
		}
		
		public function removeAllSounds(): void {
			for (var nameItem: String in sounds) {
				stopSound(nameItem);
			}
		}
		
		public function destroy(): void {
			removeAllSounds();
			sounds = null;
			instance = null;
		}
		
		private function onSoundComplete(event: Event): void {
			for (var nameItem: String in sounds) {
				if (SoundVO(sounds[nameItem]).soundChannel == event.target) {
					stopSound(nameItem);
				}
			}
		}
		
		
		private static var instance: SoundManager;
		public static function getInstance(): SoundManager {
			if (!instance) {
				instance = new SoundManager();
			}
			return instance;
		}
	}
}