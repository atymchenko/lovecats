package controller {
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.utils.Dictionary;
	
	import org.gestouch.events.GestureEvent;
	import org.gestouch.gestures.Gesture;
	import org.gestouch.gestures.TapGesture;
	import org.gestouch.gestures.TransformGesture;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	
	public class TouchManager {

		public static const CUT_TYPE: String = "cutType";
		public static const HIT_TYPE: String = "hitType";
		
		// temp public
		public var hitEnabled: Boolean;
		public var cutEnabled: Boolean;
		
		private var _type: String;
		private var stage: Stage;
		private var sprite: Sprite;
		
		private var components: Dictionary;
		private var addCompoents: Dictionary;
		
		private var killManager: KillManager;
		
		public function TouchManager() {
			components = new Dictionary(true);
			addCompoents = new Dictionary(true);
			stage = Starling.current.nativeStage;
			sprite = new Sprite();
			stage.addChild(sprite);
			killManager = KillManager.getInstance();
		}
		
		private function initGesTouchs(component: DisplayObject): Array {
			var tapGesture: TapGesture = new TapGesture(component);
			tapGesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onTap);
			var transformGesture: TransformGesture = new TransformGesture(component);
			transformGesture.addEventListener(GestureEvent.GESTURE_BEGAN, onBeganTransform);
			transformGesture.addEventListener(GestureEvent.GESTURE_CHANGED, onChnagedTransform);
			transformGesture.addEventListener(GestureEvent.GESTURE_ENDED, onEndedTransform);
			return [tapGesture, transformGesture];
		}
		
		public function register(component: DisplayObject): void {
			if (!components[component]) {
				components[component] = initGesTouchs(component);
			}
		}
		
		public function remove(component: DisplayObject): void {
			var gestures: Array = components[component];
			if (gestures) {
				for each (var gesture: Gesture in gestures) {
					gesture.removeAllEventListeners();
					gesture.dispose();
					gesture = null;
				}
				gestures = null;
				delete components[component];
			}
		}
		
		public function removeAll(): void {
			for each (var gestures: Array in components) {
				if (gestures) {
					for each (var gesture: Gesture in gestures) {
						gesture.removeAllEventListeners();
						gesture.dispose();
						gesture = null;
					}
					gestures = null;
				}
			}
			components = null;
		}
		
		public function get type(): String {
			return _type;
		}

		public function set type(value: String): void {
			_type = value;
			if (type == CUT_TYPE) {
				cutEnabled = true;
				hitEnabled = false;
			} else if (type == HIT_TYPE) {
				cutEnabled = false;
				hitEnabled = true;
			}
		}
		
		private function onTap(event: GestureEvent): void {
			if (hitEnabled) {
				var gesture: Gesture = event.target as Gesture;
				clean(); 
				sprite.graphics.beginFill(0xffffff);
				var x: Number = gesture.location.x;
				var y: Number = gesture.location.y;
				sprite.graphics.drawCircle(x, y, 15);
				sprite.graphics.endFill();
				//stage.addChild(sprite);
				trace("tap gesture: x=", x," y=",y);
				var cat: Cat = gesture.target as Cat;
				if (cat) {
					killManager.hitKill(cat);
				}
			}
		}
		
		private function onBeganTransform(event: GestureEvent): void {
			if (cutEnabled) {
				var gesture: Gesture = event.target as Gesture;
				clean();
				sprite.graphics.lineStyle(3, 0xffffff, 1);
				var x: Number = gesture.location.x;
				var y: Number = gesture.location.y;
				sprite.graphics.moveTo(x, y);
				trace("transform gesture: x=", x," y=",y);
				
				var cat: Cat = gesture.target as Cat;
				if (cat) {
					killManager.cutKill(cat);
				}
			}
		}
		
		private function onChnagedTransform(event: GestureEvent): void {
			if (cutEnabled) {
				var gesture: Gesture = event.target as Gesture;
				var x: Number = gesture.location.x;
				var y: Number = gesture.location.y;
				sprite.graphics.lineTo(x, y);
				
				var cat: Cat = gesture.target as Cat;
				if (cat) {
					killManager.cutKill(cat);
				}
			}
		}
		
		private function onEndedTransform(event: GestureEvent): void {
			if (cutEnabled) {
				//stage.addChild(sprite);
				
				var gesture: Gesture = event.target as Gesture;
				var cat: Cat = gesture.target as Cat;
				if (cat) {
					killManager.cutKill(cat);
				}
			}
		}
		
		public function clean(): void {
			sprite.graphics.clear();
		}
		
		public function destroy(): void {
			clean();
			removeAll();
			components = null;
			sprite = null;
			stage = null;
			killManager = null;
			instance = null;
		}
		
		private static var instance: TouchManager;
		public static function getInstance(): TouchManager {
			if (!instance) {
				instance = new TouchManager();
			}
			return instance;
		}

	}
}