package controller {
	import flash.utils.Dictionary;
	
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.events.Event;
	
	public class AnimationManager {
		
		private var active: Dictionary;
		
		public function AnimationManager() {
			active = new Dictionary(true);
		}
		
		public function playMC(mc: MovieClip, loop: Boolean = false, onEndAnimation: Function = null): void {
			if (mc) {
				mc.play();
				mc.loop = loop;
				mc.addEventListener(Event.ENTER_FRAME, onEnterFrame);
				Starling.juggler.add(mc);
				active[mc] = onEndAnimation;
			}
		}
		
		public function removeMC(mc: MovieClip): void {
			if (mc) {
				mc.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
				mc.stop();
				Starling.juggler.remove(mc);
				delete active[mc];
			}
		}
		
		public function pauseMC(mc: MovieClip): void {
			if (mc) {
				mc.pause();
			}
		}
		
		public function destroyMC(mc: MovieClip): void {
			if (mc) {
				if (mc.loop || !(mc.isComplete || mc.loop)) {
					removeMC(mc);
				}
				mc.dispose();
			}
		}
		
		public function playTween(target: Object, time: Number, properties: Object): void {
			Starling.juggler.tween(target, time, properties);
		}
		
		public function stopTween(target: Object): void {
			Starling.juggler.removeTweens(target);
		}
		
		/*public function playTween(target: Object, time: Number, properties: Object): void {
			var tween: Tween = new Tween(target, time);
			for (var propName: String in properties) {
				tween.animate(propName, properties[propName]);
			}
		}*/
		
		public function removeAll(): void {
			for (var item: Object in active) {
				if (item is MovieClip) {
					removeMC(item as MovieClip);
				}
			}
		}
		
		public function pauseAll(): void {
			for (var item: Object in active) {
				if (item is MovieClip) {
					pauseMC(item as MovieClip);
				}
			}
		}
		
		public function destroy(): void {
			for (var item: Object in active) {
				if (item is MovieClip) {
					destroyMC(item as MovieClip);
				}
			}
			active = null;
			instance = null;
		}
		
		private function onEnterFrame(event: Event): void {
			var target: Object = event.target;
			if (target) {
				var mc: MovieClip = target as MovieClip;
				if (mc) {
					if (mc.isComplete) {
						var onEndAnimation: Function = active[mc];
						if (onEndAnimation) {
							onEndAnimation(mc);
							if (!mc.loop) {
								removeMC(mc);
							}
						}
					}
				}
			}
		}
		
		private static var instance: AnimationManager;
		public static function getInstance(): AnimationManager {
			if (!instance) {
				instance = new AnimationManager();
			}
			return instance;
		}
	}
}