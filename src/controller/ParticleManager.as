package controller {
	import assets.AssetsParticles;
	
	import flash.geom.Point;
	
	import starling.core.Starling;
	import starling.display.DisplayObjectContainer;
	import starling.extensions.PDParticleSystem;
	import starling.extensions.ParticleSystem;
	import starling.textures.Texture;
	
	public class ParticleManager {
		
		private var scene: DisplayObjectContainer;
		
		public function ParticleManager(scene: DisplayObjectContainer) {
			this.scene = scene;
		}
		
		public function play(emitTime: Number): void {
			
		}
		
		public function get bloodParticleSystem(): ParticleSystem {
			var bloodConfig: XML = XML(new AssetsParticles.BloodConfig());
			var bloodTexture: Texture = Texture.fromBitmap(new AssetsParticles.BloodParticle());
			return new PDParticleSystem(bloodConfig, bloodTexture);
		}
		
		public function startParticle(ps: ParticleSystem, coords: Point): void {
			scene.addChild(ps);
			Starling.juggler.add(ps);
			ps.emitterX = coords.x;
			ps.emitterY = coords.y;
			ps.start();
		}
		
		public function endParticle(ps: ParticleSystem): void {
			ps.stop();
			scene.removeChild(ps);
			Starling.juggler.remove(ps);
		}
		/*
		// instantiate embedded objects
		var psConfig:XML = XML(new FireConfig());
		var psTexture:Texture = Texture.fromBitmap(new FireParticle());
		
		// create particle system
		var ps:PDParticleSystem = new PDParticleSystem(psConfig, psTexture);
		ps.x = 160;
		ps.y = 240;
		
		// add it to the stage and the juggler
		addChild(ps);
		Starling.juggler.add(ps);
		
		// change position where particles are emitted
		ps.emitterX = 20;
		ps.emitterY = 40;
		
		// start emitting particles
		ps.start();*/
	}
}