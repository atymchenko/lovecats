package {
	
	import controller.AnimationManager;
	import controller.TouchManager;
	
	import starling.display.MovieClip;
	import starling.display.Sprite;
	
	public class Cat extends Sprite {
		
		private var mc: MovieClip;
		
		private var onStartHandler: Function;
		private var onEndHandler: Function;
		
		private var touchManager: TouchManager = TouchManager.getInstance();
		private var animationManager: AnimationManager = AnimationManager.getInstance();
		
		private var _isDisposed: Boolean;
		public function get isDisposed(): Boolean {
			return _isDisposed;
		}
		
		public function Cat(mc: MovieClip, onStartHandler: Function = null, onEndHandler: Function = null) {
			super();
			this.mc = mc;
			this.onStartHandler = onStartHandler;
			this.onEndHandler = onEndHandler;
			addChild(mc);
			addListeners();
			animationManager.playMC(mc, true);
		}
		
		private function addListeners(): void {
			if (touchManager) {
				touchManager.register(this);
			}
		}
		
		private function removeListeners(): void {
			if (touchManager) {
				touchManager.remove(this);
			}
		}
		
		public function kill(): void {
			if (!isDisposed && mc.isPlaying) {
				animationManager.removeMC(mc);
				removeListeners();
				if (onStartHandler != null) {
					onStartHandler(this);
				}
				trace("Cat killed: "+this);
			}
		}
		
		override public function dispose(): void {
			kill();
			removeChild(mc);
			mc.dispose();
			mc = null;
			parent.removeChild(this);
			touchManager = null;
			super.dispose();
			_isDisposed = true;
			trace("Cat disposed: "+this);
		}
	}
}